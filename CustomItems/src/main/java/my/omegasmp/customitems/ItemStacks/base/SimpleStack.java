package my.omegasmp.customitems.ItemStacks.base;

import org.bukkit.inventory.ItemStack;

public abstract class SimpleStack {

    public abstract void init();

    public abstract ItemStack getItemStack();

    public abstract double getID();
}
