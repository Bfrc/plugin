package my.omegasmp.customitems.ItemStacks.items;

import my.omegasmp.customitems.ItemStacks.base.SimpleStack;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class EnchantedGoldBlock extends SimpleStack {
    private ItemStack itemStack = new ItemStack(Material.GOLD_BLOCK);

    @Override
    public void init() {

    }

    @Override
    public ItemStack getItemStack() {
        return null;
    }

    @Override
    public double getID() {
        return 0;
    }
}
