package my.omegasmp.worldgen;

import my.omegasmp.worldgen.generator.CustomChunkGenerator;
import org.bukkit.Bukkit;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

public final class WorldGen extends JavaPlugin {

    @Override
    public void onEnable() {
        System.out.println("Omega Smp custom generator");
        Bukkit.getPluginManager().registerEvents(new onBreak(), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return new CustomChunkGenerator();
    }

}
