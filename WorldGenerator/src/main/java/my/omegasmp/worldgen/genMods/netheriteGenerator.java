package my.omegasmp.worldgen.genMods;

import my.omegasmp.worldgen.generator.other.GenMod;
import my.omegasmp.worldgen.generator.other.GenerationMod;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

public class netheriteGenerator implements GenerationMod {
    private Random random;

    @GenMod
    public ChunkGenerator.ChunkData populate(World world, Random random, int chunkX, int chunkZ, ChunkGenerator.BiomeGrid biome, ChunkGenerator.ChunkData data) {
        this.random = random;
        int X, Y, Z;
        int glowstoneAmount = getRandomNumberUsingInts(10, 20);
        while (glowstoneAmount != 0) {
            X = getRandomNumberUsingInts(1, 16);
            Z = getRandomNumberUsingInts(1, 16);
            Y = getRandomNumberUsingInts(1, 30);
            if (!data.getType(X, Y, Z).equals(Material.STONE)) {
                continue;
            }
            data.setBlock(X, Y, Z, Material.ANCIENT_DEBRIS);
            --glowstoneAmount;
        }
        return data;
    }

    public int getRandomNumberUsingInts(int min, int max) {
        return random.ints(min, max)
                .findFirst()
                .getAsInt();
    }
}
