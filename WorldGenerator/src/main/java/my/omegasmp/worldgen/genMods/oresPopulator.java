package my.omegasmp.worldgen.genMods;

import my.omegasmp.worldgen.generator.other.GenMod;
import my.omegasmp.worldgen.generator.other.GenerationMod;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

//data = chunk
public class oresPopulator implements GenerationMod {
    static int CNT = 4;
    private Random random;

    @GenMod
    public ChunkGenerator.ChunkData populate(World world, Random random, int chunkX, int chunkZ, ChunkGenerator.BiomeGrid biome, ChunkGenerator.ChunkData data) {
        this.random = random;
        int X, Y, Z;
        boolean isStone;
        int vanesAmount = getRandomNumberUsingInts(100, 500);
        while (vanesAmount != 0) {
            if (getRandomNumberUsingInts(1, 10) > 5) {
                continue;
            }
            X = getRandomNumberUsingInts(1, 16);
            Z = getRandomNumberUsingInts(1, 16);
            Y = getRandomNumberUsingInts(1, 253);

            if (!data.getType(X, Y, Z).equals(Material.STONE)) {
                continue;
            }

            int oreChance = getRandomNumberUsingInts(1, 1000);
            Material material = Material.COAL_ORE;
            Material material_heart = Material.COAL_BLOCK;
            if (oreChance < 250) { //25%
                material = Material.IRON_ORE;
                material_heart = Material.IRON_BLOCK;
            }
            if (oreChance < 125) { //12,5%
                material = Material.GOLD_ORE;
                material_heart = Material.GOLD_BLOCK;
            }
            if (oreChance < 50) { //5
                material = Material.DIAMOND_ORE;
                material_heart = Material.DIAMOND_BLOCK;
            }
            if (oreChance < 25) { //2.5
                material = Material.REDSTONE_ORE;
                material_heart = Material.REDSTONE_BLOCK;
            }

            int vein_length = 14;

            //ELONGATED BLOB
            float max_change = 0.9F;
            int off_stone = 3;
            //intentional
            //float dir[3] = {0.0F, 0.0F, 0.0F};

            float[] dir = new float[3];
            for (int i = 0; i < vein_length; i++) {
                dir[0] = 0F;
                dir[1] = 0F;
                dir[2] = 0F;
                switch (getRandomNumberUsingInts(1, 3)) {
                    case 1:
                        dir[0] += (float) (getRandomNumberUsingInts(-1, 1));
                        break;
                    case 2:
                        dir[1] += (float) (getRandomNumberUsingInts(-1, 1));
                        break;
                    case 3:
                        dir[2] += (float) (getRandomNumberUsingInts(-1, 1));
                        break;
                    default:
                        throw new IllegalStateException("Unexpected random number");
                }
                if (data.getType(
                        X + (int) (dir[0]),
                        Y + (int) (dir[1]),
                        Z + (int) (dir[2])) != Material.STONE) {

                    off_stone--;

                    if (off_stone == 0 ||
                            data.getType(
                                    X + (int) (dir[0]),
                                    Y + (int) (dir[1]),
                                    Z + (int) (dir[2])) != Material.AIR) {
                        break;
                    }
                }


                data.setBlock(X + (int) (dir[0]), Y + (int) (dir[1]), Z + (int) (dir[2]), material);

                --vanesAmount;
            }
        }

        return data;
    }

    public int getRandomNumberUsingInts(int min, int max) {
        return random.ints(min, max)
                .findFirst()
                .getAsInt();
    }


}