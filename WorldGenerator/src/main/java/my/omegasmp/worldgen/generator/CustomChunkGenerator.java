package my.omegasmp.worldgen.generator;

import my.omegasmp.worldgen.utils.FastNoiseLite;
import my.omegasmp.worldgen.utils.ModMenger;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

public class CustomChunkGenerator extends ChunkGenerator {
    private FastNoiseLite gen;
    private ModMenger modMenger;

    @Override
    public ChunkData generateChunkData(World world, Random random, int chunkX, int chunkZ, BiomeGrid biome) {
        if (gen == null) {
            gen = new FastNoiseLite((int) world.getSeed());
            gen.SetNoiseType(FastNoiseLite.NoiseType.ValueCubic);
            gen.SetFractalType(FastNoiseLite.FractalType.FBm);
            gen.SetFractalOctaves(1);
            gen.SetFrequency(0.05F);
        }
        if (modMenger == null) {
            modMenger = new ModMenger("my.omegasmp.worldgen.genMods");
        }
        ChunkData chunk = createChunkData(world);
        for (int X = 0; X < 16; X++) {
            for (int Z = 0; Z < 16; Z++) {
                int currentHeight = 256;//(int) ((gen.GetNoise(chunkX * 16 + X, 0, chunkZ * 16 + Z) + 1 ) * 15D + 50D);
                chunk.setBlock(X, currentHeight, Z, Material.STONE);
                for (int Y = 0; Y < currentHeight; Y++) {
                    if (Y == 0) {
                        chunk.setBlock(X, Y, Z, Material.BEDROCK);
                        continue;
                    }
                    if (Y == 255) {
                        chunk.setBlock(X, Y, Z, Material.RED_WOOL);
                        continue;
                    }
                    float f = gen.GetNoise(X + chunkX * 16, Y, Z + chunkZ * 16);
                    if (f > -0.12) {
                        chunk.setBlock(X, Y, Z, Material.STONE);
                    }


                }
            }
        }
        chunk = modMenger.callGeneration(world, random, chunkX, chunkZ, biome, chunk);
        return chunk;

    }


}