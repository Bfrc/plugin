package my.omegasmp.worldgen;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class onBreak implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (!(block.getType() == Material.RED_WOOL)) {
            return;
        }
        int Y = block.getLocation().getBlockY();
        if (Y == 255) {
            event.setCancelled(true);
        }
    }
}
