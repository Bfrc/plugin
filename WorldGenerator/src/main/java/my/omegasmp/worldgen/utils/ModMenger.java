package my.omegasmp.worldgen.utils;

import my.omegasmp.worldgen.generator.other.GenMod;
import my.omegasmp.worldgen.generator.other.GenerationMod;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class ModMenger {

    private ArrayList<Class<? extends GenerationMod>> mods = new ArrayList<>();
    private HashMap<Method, Object> classInstances = new HashMap<>();

    public ModMenger(String path) {
        Reflections reflections = new Reflections(path, new SubTypesScanner());
        mods.addAll(reflections.getSubTypesOf(GenerationMod.class));
    }

    public ChunkGenerator.ChunkData callGeneration(World world, Random random, int chunkX, int chunkZ, ChunkGenerator.BiomeGrid biome, ChunkGenerator.ChunkData data) {
        for (Class<? extends GenerationMod> clasz : mods) {
            try {
                for (Method method : clasz.getMethods()) {
                    Object object1 = classInstances.get(method);
                    if (object1 != null) {
                        data = (ChunkGenerator.ChunkData) method.invoke(object1, world, random, chunkX, chunkZ, biome, data);
                        continue;
                    }
                    if (method.getAnnotation(GenMod.class) == null) {
                        continue;
                    }
                    Object object = clasz.newInstance();
                    data = (ChunkGenerator.ChunkData) method.invoke(object, world, random, chunkX, chunkZ, biome, data);
                    classInstances.put(method, object);
                }
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return data;
    }
}
